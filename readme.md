# PIC 10B: Intermediate programming (fall 2018)

This is the _unofficial_ class website. Here you will find handouts, slides, and
code related to the concepts we discuss during lecture.


## Sections

*   Material hosted at the PIC web server
    -   [Home-made classes][hmc]
    -   [Handouts][handouts]
    -   [Main topics summaries][summary]

*   Material hosted at CCLE
    -   [Course assignments][hw]
    -   [Discussion materials][ds]
    -   [Practice problems (includes old exams)][pp]

*   _Deprecated content_
    -   [Old lessons (Google slides)][slides]
    -   [Old examples (code)][code]
    -   [Other resources][other]

[hmc]: classes
[handouts]: handouts
[summary]: summary
[hw]: https://ccle.ucla.edu/course/view.php?id=65107&section=2
[ds]: https://ccle.ucla.edu/course/view.php?id=65107&section=3
[pp]: https://ccle.ucla.edu/course/view.php?id=65107&section=4
[slides]: https://ccle.ucla.edu/course/view.php?id=65107&section=5
[code]: https://ccle.ucla.edu/course/view.php?id=65107&section=6
[other]: https://ccle.ucla.edu/course/view.php?id=65107&section=7


## Additional resources

*   [The class syllabus][syllabus]
*   [The _official_ CCLE class website][CCLE]
*   [Assignment scores and comments][four-o-four]. You need a _login_ and
    _password_ to access this site.


[syllabus]: syllabus
[CCLE]: https://ccle.ucla.edu/course/view/18F-COMPTNG10B-1
[four-o-four]: https://www.kualo.co.uk/404/
[lego404]: http://www.lego.com/404notfound


