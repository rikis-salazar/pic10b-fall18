#ifndef DL_DECLARATIONS_H
#define DL_DECLARATIONS_H

template <typename T>
class Node;

template <typename T>
class Iterator;

template <typename T>
class DoublyLinkedList;

#endif
