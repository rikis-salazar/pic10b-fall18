#include "dl_list.h"

#include <iostream>

using std::cout;
using std::ostream;

template <typename T>
ostream& operator<<( ostream& out, DoublyLinkedList<T> l ){
    out << "Contents of list:\n";         // ^^^^^ By value to test copy constr.

    Iterator<T> it = l.first() ;
    while ( !it.is_null() ){
        out << it.get_value() << " ";
        it.move_forward();
    }

    return out;
}

int main(){
    DoublyLinkedList<int> l;

    l.push_back(10);
    l.push_back(20);
    l.push_back(30);

    Iterator<int> it = l.first();
    cout << "First: " << it.get_value() << '\n';

    it.move_forward();
    cout << "Second: " << it.get_value() << '\n';

    it = l.last();
    cout << "Last: " << it.get_value() << '\n';

    l.pop_front();
    cout << l << '\n';

    return 0;
}
