# Course handouts

The following documents contain information that might come in handy at some
point.

*   [Making sure your project compiles before you submit it to CCLE][compile].
*   [Figuring out your _working directory_][wd].
*   [The big 4][big4].
*   [Our very own non-template vector][vector].
*   [A template doubly linked list class][list].
*   [Container adapters: _stacks_ & _queues_][adapters].

[compile]: ./hw0-tester-handout.html
[wd]: ./data-files-vs.html
[big4]: ./the-big-4.html
[vector]: ./non-template-vector.html
[list]: ./template-list.html
[adapters]: ./container-adapters.html

---

[Return to main PIC course website][main]

[main]: ..

