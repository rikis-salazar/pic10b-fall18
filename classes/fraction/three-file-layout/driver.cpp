#include <iostream>
#include <algorithm>
#include <vector>
#include "pic10b_fraction.h"

// using statements are also OK in test files. However, consider limiting their
// scope (see main below).
//     using std::cout;
//     using std::vector;
//     using std::sort;


int main(){
    using std::cout;
    using std::vector;
    using std::sort;

    cout << "Hello, 'Fractioned' World!\n\n";

    cout << "Final tests ...\n"; 
    Fraction f1;         // 0/1 
    Fraction f2(2);      // 2/1 
    Fraction f3(3, -6);  // (-1)/2 

    cout << "Fraction f1;    Fraction f2(2);    "; 
    cout << "Fraction f3(3, -6);\nProduces:\t"; 
    cout << "f1 = " << f1 << "\t"; 
    cout << "f2 = " << f2 << "\t"; 
    cout << "f3 = " << f3 << "\n"; 

    cout << "\nExpression\t\tResult\t\tOperator\n"
         << "  -f3\t\t\t" << -f3 << "\t\tunary (-)\n"
         << "  2 - f3\t\t" << Fraction(2) - f3 << "\t\tbinary (-)\n"
         << "  -( f3 - 2 )\t\t" << -( f3 - Fraction(2) )
         << "\t\tunary & binary (-)\n";

    f1 = f1 + f3;
    cout << "  f1 = f1 + f3\t\tf1 = " << f1 << "\t(+)\n"
         << "  f2 * f3\t\t" << f2 * f3 << "\t\t(*)\n";

    Fraction f4 = f1 / f2;
    cout << "  f4 = f1 / f2\t\tf4 = " << f4 << "\t(/)\n\n";

    f4 += 3;
    cout << "  f4 += 3\t\tf4 = " << f4 << "\t(+=)\n";

    f4 -= f2;
    cout << "  f4 -= f2\t\tf4 = " << f4 << "\t(-=)\n";

    f2 *= f4;
    cout << "  f2 *= f4\t\tf2 = " << f2 << "\t(*=)\n";

    f1 /= f2;
    cout << "  f1 /= f2\t\tf1 = " << f1 << "\t(/=)\n";

    cout << "\nf1 holds the value " << f1 << ".\n";
    cout << "The statement  ' cout << f1++ ; '  displays the value "
	<< f1++ << ",\n";
    cout << "and afterwards f1 holds the value " << f1 << ".\n";

    cout << "\nOn the other hand, f4 holds the value " << f4 << ".\n";
    cout << "The statement  ' cout << ++f4 ; '  displays the value "
	<< ++f4 << ",\n";
    cout << "and afterwards f4 holds the value " << f4 << ".\n";

    cout << "\nFractions to decimals:"
         << "\nf1 = " << static_cast<double>(f1) 
         << "\nf2 = " << static_cast<double>(f2) 
         << "\nf3 = " << static_cast<double>(f3) 
         << "\nf4 = " << static_cast<double>(f4)
         << '\n';

    vector<Fraction> v(4);
    v[0] = f1;
    v[1] = f2;
    v[2] = f3;
    v[3] = f4;

    // This function uses 'operator<' to sort the fractions
    sort(v.begin(),v.end());

    cout << "\nFrom smaller to bigger values:";
    for ( auto f : v )
        cout << '\n' << f;
    cout << '\n';

    return 0;
}

/** 
   Output:

        Hello, 'Fractioned' World!

        Final tests ...
        Fraction f1;    Fraction f2(2);    Fraction f3(3, -6);
        Produces:	f1 = 0/1	f2 = 2/1	f3 = (-1)/2

        Expression		Result		Operator
          -f3			1/2		unary (-)
          2 - f3		5/2		binary (-)
          -( f3 - 2 )		5/2		unary & binary (-)
          f1 = f1 + f3		f1 = (-1)/2	(+)
          f2 * f3		(-1)/1		(*)
          f4 = f1 / f2		f4 = (-1)/4	(/)

          f4 += 3		f4 = 11/4	(+=)
          f4 -= f2		f4 = 3/4	(-=)
          f2 *= f4		f2 = 3/2	(*=)
          f1 /= f2		f1 = (-1)/3	(/=)

        f1 holds the value (-1)/3.
        The statement  ' cout << f1++ ; '  displays the value (-1)/3,
        and afterwards f1 holds the value 2/3.

        On the other hand, f4 holds the value 3/4.
        The statement  ' cout << ++f4 ; '  displays the value 7/4,
        and afterwards f4 holds the value 7/4.

        Fractions to decimals:
        f1 = 0.666667
        f2 = 1.5
        f3 = -0.5
        f4 = 1.75

        From smaller to bigger values:
        (-1)/2
        2/3
        3/2
        7/4
**/
