% Lecture 2 (Summary)
% Ricardo Salazar
% January 10, 2017


# The theory 

## What we did

During lecture we

 *  implemented _getter_ functions for our `Fraction` class;

 *  declared [but did not define] class _setters_;

 *  experimented with different ways to `print()` fractions;

 *  implemented functions that return member fields **by reference** (they can
    be used as setters or getters);

 *  failed to be `const`-correct...  and faced the consequences.


## Concepts we came across:

 i. `const`-**correctness**:

    > if a function does not alter the values of member fields, it should
    > advertise itself as being `const`.

 i. **Return by reference**:
 
    > simply append `&` to the return type. E.g.:  
    >
    > ~~~~~ {.cpp}
    >     int& numerator();   
    > ~~~~~ 
    >
    > A technique that should be used very carefully.

 i. **Overloading by `const` modifier**:  
 
    > two functions whose _signatures_ differ only by the presence of the 
    > `const` modifier (the compiler will choose the `const` version only when
    > strictly needed).
    

## The goals

We want to: 

 i. Establish a **single point of maintenance** for the class constructors.
    
 i. Code more efficient constructors.

 i. Overload `operator<<` so that `Fraction` objects can be _sent to_ the
    console (_e.g._ via `std::cout`).

 i. Favor **readable** code over _smart_/_compact_ code.

# The code

## The interface

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class Fraction{
    ...       // <-- Member fields and constructors

  public:
    int get_numer() const;
    int get_denom() const;

    int& numerator();
    int numerator() const;

    void print() const;
    ...       // <-- Setters, `denominator()` and `print_to()`
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Function definitions

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
// Getters
int Fraction::get_numer() const {
    return numer;
}

int Fraction::get_denom() const {
    return denom;
}

// Setters
//     You code them!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Function definitions (cont)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
// Overload by `const` modifier
int& Fraction::numerator() {
    return numer;
}

int Fraction::numerator() const {
    return numer;
}

// Alternatively:
//     const int& Fraction::numerator() const {
//         return numer;
//     }
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Function definitions (cont)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
// For didactic purposes we tried different ways.
void Fraction::print() const {
    // This works OK
    cout << numer << '/' << denom;

    // These fail if the involved functions are not `const`
    // cout << numer << '/' << get_denom();
    // cout << numerator() << '/' << denom;

    return;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# Other notes

## Issues & warnings

Issues:

 *  Code is still very repetitive and inefficient.

Warnings:

 *  Return by reference can be dangerous. If the reference is no longer _within
    reach_ (_e.g.,_ the object/variable is out of scope), the program can crash.

> We'll soon discuss a function that cannot return by value. As well as other
> functions that benefit from this technique.


## Wishlist

 *  This would be nice

    > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    > Fraction f(1,2);
    > cout << "The value of f is " << f << '.';
    > // Output:
    > //     The value of f is 1/2.
    > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 *  A working set of mathematical operations.

    - Arithmetic operations: `+`, `-`, `*`, `/`.
    - Increment/decrement operations: `++`, `--`.
    - Shortcut-like operations: `+=`, `-=`, `*=`, `/=`.
    - Boolean operators: `<`, `>`, `<=`, `>=`, `==`, `!=`.


## Compiler _sees_ ...

In `C++`, simple expressions like: `x + y`, `v[0]`, `z = 2`, and `cout << a`;
have equivalent _function-like_ versions.

 -  Depending on the types of `x` and `y`, the sum `x + y` can be interpreted as
    either `operator+(x,y)`, or `x.operator+(y)`.
 -  If `v` is a vector, then `v[0]` is interpreted as `v.operator[](0)`.
 -  Assuming `z` is an object that has been _constructed_ beforehand, `z = 2`
    is _seen_ as `z.operator=(2)`.
 -  Lastly, `cout << a` is seen as `operator<<(cout,a)`.


## Compiler _sees_ ... (cont)

The last statement in the previous slide is particularly telling.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
cout << a;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The variable `a` **is not** _sent to_ "the function" `cout`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
operator<<( cout, a );
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Instead both `cout`, and `a`, are _sent to_ the function `operator<<`.

> **Question:**
>
> _How would you make sense of the **chained** sequence below?_
>
> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
> cout << "Value of a: " << a;
> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


# Resources

## Code from lecture

|**File**|**Description**|**URL**|
|:---------------:|:------------------------------------------|:---------------|
| [`fraction.cpp`][00-example] | Code from lecture. | [`cpp.sh/4vq52`][00-url] |
| [`cle...on.cpp`][01-example] | Code from lecture (clean version). | [`cpp.sh/6nl26`][01-url] |

[00-example]: lec2_code/fraction.cpp
[01-example]: lec2_code/clean-fraction.cpp
[00-url]: http://cpp.sh/4vq52
[01-url]: http://cpp.sh/6nl26
