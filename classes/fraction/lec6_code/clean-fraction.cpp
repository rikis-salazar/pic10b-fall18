#include <iostream>          // std::cout, std::ostream

// using statements
using std::cout;
using std::ostream;


class Fraction{
  private:
    int numer;
    int denom;
    // void normalize();     // My implementation will appear in the final
                             // version of this class.
  public:
    // constructor(s)
    Fraction( int = 0, int = 1 );

    // Access to private fields
    int& numerator();
    int& denominator();
    int numerator() const;
    int denominator() const;

    // Operators can also be **MEMBERS**. 
    Fraction operator-() const ;

    // These operators allow for chaining (hence the return by reference)
    Fraction& operator+=( const Fraction& );
    Fraction& operator-=( const Fraction& );
    Fraction& operator*=( const Fraction& );
    Fraction& operator/=( const Fraction& );

    // Pre-fix increment/decrement operators can also be chained.
    Fraction& operator++();
    Fraction& operator--();

    // Post-fix operators cannot be chained. Moreover, they return a copy of the
    // old value stored in the Fraction object.
    Fraction operator++( int );
    Fraction operator--( int );

    // Conversion operator(s). Notice that the return type is "missing"
    operator double() const;
    operator bool() const;

    // Friend **NON-MEMBER** functions and/or operators.
    friend ostream& operator<<( ostream&, const Fraction& );
    friend int compare( const Fraction&, const Fraction& );
    // NOTE:
    // There is no need for friends! This is mostly done for didactic purpose.
};

// NON-MEMBER, NON-FRIEND OPERATORS
Fraction operator+( const Fraction&, const Fraction& );
Fraction operator-( const Fraction&, const Fraction& );
Fraction operator*( const Fraction&, const Fraction& );
Fraction operator/( const Fraction&, const Fraction& );

/* ********************** end of declarations ******************** */


// Constructor(s)
Fraction::Fraction( int n, int d )
  : numer(n), denom(d) {
    // Common factors in the numerator and denominator should cancel each other
    // out. In addition, the sign of the fraction (if any) should remain with
    // the numerator. For example, the object `Fraction f(2,-4)` should be
    // stored as (-1)/2. In other words, we should have `f.numer = -1`, and
    // `f.denom = 2`.
    //
    // normalize();
}


// Getter-setter functions
int& Fraction::numerator() {
    return numer;
}
int& Fraction::denominator() {
    return denom;
}
int Fraction::numerator() const {
    return numer;
}
int Fraction::denominator() const {
    return denom;
}


// Member unary minus
Fraction Fraction::operator-() const { 
    return Fraction( -numer, denom );
}


// Member operators that modify the implicit object
Fraction& Fraction::operator+=( const Fraction& rhs ){
    *this = *this + rhs;   // This calls a member function WE DID NOT implement!
    return *this;          // It is a member of the so called "Big 3"
}
Fraction& Fraction::operator-=( const Fraction& rhs ){
    *this = *this - rhs;
    return *this;
}
Fraction& Fraction::operator*=( const Fraction& rhs ){
    *this = *this * rhs;
    return *this;
}
Fraction& Fraction::operator/=( const Fraction& rhs ){
    *this = *this / rhs;
    return *this;
}


// Since `++f` is equivalent to `f += 1`, we reuse the `+=` operator.
Fraction& Fraction::operator++(){      // <-- PREFIX version
    return *this += 1;
}
// Likewise with `--f`
Fraction& Fraction::operator--(){      // <-- PREFIX version
    return *this -= 1;
}

// Recall that `f++` has the net effect of `++f`, that is, the fraction `f`
// increases by one. However, the old (non-increased) value of `f` is the one
// that is returned.
Fraction Fraction::operator++( int unused ){     // <-- POSTFIX version
    Fraction clone(*this); // This is a call to another member function WE DID
    ++(*this);             // NOT implement! Another member of the "Big 3".
    return clone;
}
Fraction Fraction::operator--( int unused ){ // <-- POSTFIX version
    // We can make a copy of the implicit object via the two parameter
    // constructor (which we coded ourselves). However, there is still an
    // 'invisible' member of the "Big 3" that is being called within this
    // operator.
    Fraction clone( numer, denom );
    --*this;      // There is no need for parenthesis
    return clone;
}

/* **************** end of member fun. definitions ***************** */

Fraction::operator double() const {
    return static_cast<double>( numer ) / denom;
}

Fraction::operator bool() const {
    return numer;       // Implicitly converted to bool.
}                       // false if zero; true otherwise.

/* ************** end of member conversion operators *************** */

// Non-member friend operator(s)
ostream& operator<<( ostream& out, const Fraction& f ){
    out << f.numer << '/' << f.denom;   // <-- Requires friendship
    return out;
}

// Non-member friend function(s)
int compare( const Fraction& lhs, const Fraction& rhs ) {
    // Recall that
    //     p/q < r/s   <==>   p/q - r/s < 0.
    //
    // In addition, we have
    //     p/q - r/s = ( p*s - r*q ) / q*s.
    //
    // Assuming both denominators are positive (i.e., the Fraction objects are
    // _normalized_, then
    //     p/q - r/s < 0   <==>   p*s - r*q < 0
    //
    // Summarizing:
    //     p/q <  r/s   <==>   p*s - r*q < 0
    //
    // Similarly, the outcome of all comparison between p/q and r/s is
    // determined by the int value p*s - r*q.
    //     p/q >  r/s   <==>   p*s - r*q > 0
    //     p/q <= r/s   <==>   p*s - r*q <= 0
    //     p/q >= r/s   <==>   p*s - r*q >= 0
    //     p/q == r/s   <==>   p*s - r*q == 0
    //     p/q != r/s   <==>   p*s - r*q != 0
    //
    // Hence this expression becomes the perfect candidate for the return of
    // this helper function.

    int p = lhs.numer;
    int q = lhs.denom;
    int r = rhs.numer;
    int s = rhs.denom;
    return p*s - r*q;
}

/* **************** end of friend fun. definitions ***************** */
bool operator<( const Fraction& f , const Fraction& g ){
    return compare(f,g) < 0 ;
}
bool operator>( const Fraction& f , const Fraction& g ){
    return compare(f,g) > 0 ;
}
bool operator<=( const Fraction& f , const Fraction& g ){
    return compare(f,g) <= 0 ; 
}
bool operator>=( const Fraction& f , const Fraction& g ){
    return compare(f,g) >= 0 ;
}
bool operator==( const Fraction& f , const Fraction& g ){
    return compare(f,g) == 0 ;
}
bool operator!=( const Fraction& f , const Fraction& g ){
    return compare(f,g) != 0 ;
}

/* ***************** end of boolean operators ******************* */

// Non-member, non-friend operators
Fraction operator+( const Fraction& lhs, const Fraction& rhs ){
    // Recall:    p/q + r/s = ( p*s + q*r ) / q*s;
    int p = lhs.numerator();
    int q = lhs.denominator();
    int r = rhs.numerator();
    int s = rhs.denominator();

    int top = p*s + q*r;
    int bottom = q*s;

    return Fraction( top, bottom );
}

Fraction operator-( const Fraction& lhs, const Fraction& rhs ){
    // Binary minus reuses unary minus, as well as `operator+`
    return lhs + (-rhs);

    // The compiler sees:
    //     return operator+( lhs, rhs.operator-() );
}

Fraction operator*( const Fraction& lhs, const Fraction& rhs ){
    // Recall:    p/q * r/s = p*r / q*s;
    int p = lhs.numerator();
    int q = lhs.denominator();
    int r = rhs.numerator();
    int s = rhs.denominator();

    return Fraction( p*r, q*s );
}

Fraction operator/( const Fraction& lhs, const Fraction& rhs ){
    // Recall:    (p/q) / (r/s) = p/q * s/r ;
    //                                  ^^^   <-- inverse of (r/s)
    Fraction rhs_inverse( rhs.denominator(), rhs.numerator() );

    return lhs * rhs_inverse;
}

/* ********************** end of definitions ******************** */



// The so-called DRIVER (or tester)
int main(){
    Fraction half(1,2);
    Fraction third(1,3);
    Fraction almost_one(1023,1024);

    cout << "One half: " << half << '\n';
    cout << "One third: " << third << '\n';
    cout << "Almost one: " << almost_one << "\n\n";


    // Implicit conversion to bool
    if ( !Fraction() )
        cout << "Fraction() defaults to 0/1.\n";

    // Explicit conversion to double...
    if ( double(half) < 3.14  ) // <-- Not one of our boolean comparators
        cout << half << " < " << 3.14 << ". Now, this makes sense.\n\n";

    // The implicit one is ambiguous.
    //     if ( half < 3.14 )
    // The compiler doesn't know how to compare them. Possible options include:
    //     if ( bool(half) < 3.14 )      // int vs double
    //     if ( double(half) < 3.14 )    // double vs double 
    //     if ( half < Fraction(3.14) )  // Fraction vs Fraction (via truncation
    //                                   // of double, followed by call to class
    //                                   // constructor).


    // Increment/decrement operators
    Fraction not_half = half;

    cout << "Now you half: " << not_half++; 
    cout << ", now you don't: " << ++not_half << "\n";
    cout << "This is fun, let's do it again.\n";

    cout << "Now you half: " << ----not_half;
    cout << ", now you... [also?!] half:" << not_half--;
    cout << ", and now you don't: " << ( not_half -= half ) << "\n\n";


    // Chaining
    not_half = half;
    Fraction not_third = third;

    cout << "What's going on here? " << ( not_third *= not_half += almost_one );
    cout << " <--- ????\n\n";
}
