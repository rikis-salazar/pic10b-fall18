#include <iostream>          // std::cout, std::ostream
#include <fstream>           // std::ofstream

// using statements
using std::cout;
using std::ostream;
using std::ofstream;
using std::string;

class Fraction{
  private:
    int numer;
    int denom;
    string name;

  public:
    // constructor(s)
    Fraction( int = 0, int = 1 );

    // Access to private fields
    int& numerator();
    int& denominator();
    int numerator() const;
    int denominator() const;

    // Operators can also be **MEMBERS**. For now, to take advantage of implicit
    // conversions we'll code all symmetric operators (i.e., `+`, `*`, `/`, and
    // binary minus `-`) as non members. Since the unary minus operator cannot
    // take advantage of implicit conversion, we'll code it as a member.
    Fraction operator-() const ;

    Fraction& operator+=( const Fraction& rhs);
    Fraction& operator-=( const Fraction& rhs);
    Fraction& operator*=( const Fraction& rhs);
    Fraction& operator/=( const Fraction& rhs);

    Fraction& operator++(){
        return *this += 1;
    }

    Fraction operator++(int unused){
        Fraction clone(*this);
        // *this += 1;
        // *this++;
        ++(*this);

        return clone;

    }

    // Friend **NON-MEMBER** functions and operators. Since we have a perfectly
    // working public interface, there is really no need to "have friends".
    // However, for didactic purposes, we'll keep `operator<<` as a friend.
    friend ostream& operator<<( ostream&, const Fraction& );
};


// There is no need to, first declare and then define non-member operators.
// However, to keep our layout "consistent" we'll do just that: declare here,
// and define elsewhere.
Fraction operator+( const Fraction&, const Fraction& );
Fraction operator-( const Fraction&, const Fraction& );
Fraction operator*( const Fraction&, const Fraction& );
Fraction operator/( const Fraction&, const Fraction& );


/* ********************** end of declarations ******************** */

// Efficient constructor
Fraction::Fraction( int n, int d ) : numer(n), denom(d) {
    // normalize();          // See textbook for details.
}


// Getter-setter functions
int& Fraction::numerator() {
    return numer;
} 
int& Fraction::denominator() {
    return denom;
} 
int Fraction::numerator() const {
    return numer;
} 
int Fraction::denominator() const {
    return denom;
} 


// Member unary minus
Fraction Fraction::operator-() const { 
    return Fraction( -numer, denom );
}

/* **************** end of member fun. definitions ***************** */


// Non-member friend operators
ostream& operator<<( ostream& out, const Fraction& f ){
    out << f.numer << '/' << f.denom;   // <-- Requires friendship
    return out;
}


/* **************** end of friend fun. definitions ***************** */

// Non-member, non-friend operators
Fraction operator+( const Fraction& lhs, const Fraction& rhs ){
    // Recall:    p/q + r/s = ( p*s + q*r ) / q*s;
    int p = lhs.numerator();
    int q = lhs.denominator();
    int r = rhs.numerator();
    int s = rhs.denominator();

    int top = p*s + q*r;
    int bottom = q*s;

    return Fraction( top, bottom );
}

Fraction operator-( const Fraction& lhs, const Fraction& rhs ){
    // Binary minus reuses unary minus, as well as `operator+`
    return lhs + (-rhs);

    // The compiler sees:
    //     return operator+( lhs, rhs.operator-() );
}

Fraction operator*( const Fraction& lhs, const Fraction& rhs ){
    // Recall:    p/q * r/s = p*r / q*s;
    int p = lhs.numerator();
    int q = lhs.denominator();
    int r = rhs.numerator();
    int s = rhs.denominator();

    return Fraction( p*r, q*s );
}

Fraction operator/( const Fraction& lhs, const Fraction& rhs ){
    // Recall:    (p/q) / (r/s) = p/q * s/r ;
    //                                  ^^^   <-- inverse of (r/s)
    Fraction rhs_inverse( rhs.denominator(), rhs.numerator() );

    return lhs * rhs_inverse;
}

/* ********************** end of definitions ******************** */

int compare( const Fraction& lhs, const Fraction& rhs){
    int p = lhs.numerator();
    int q = lhs.denominator();
    int r = rhs.numerator();
    int s = rhs.denominator();
// p/q < r/s   <==> p/q - r/s < 0
// ( p * s - q * r ) / qs < 0
// p * s - q * r < 0
    return p * s - q * r;


}                   // the outcome of ALL boolean operators. 

/* ************************* end of stubs *********************** */

bool operator<( const Fraction& f , const Fraction& g ){
    return compare(f,g) < 0 ;
} 
bool operator>( const Fraction& f , const Fraction& g ){
    return compare(f,g) > 0 ;
} 
bool operator<=( const Fraction& f , const Fraction& g ){
    return compare(f,g) <= 0 ; 
} 
bool operator>=( const Fraction& f , const Fraction& g ){
    return compare(f,g) >= 0 ;
} 
bool operator==( const Fraction& f , const Fraction& g ){
    return compare(f,g) == 0 ;
} 
bool operator!=( const Fraction& f , const Fraction& g ){
    return compare(f,g) != 0 ;
} 


Fraction& Fraction::operator+=( const Fraction& rhs ){
    // numer += ...
    // denom += denom * rhs.denom;
    *this = *this + rhs;

    // return Fraction(top,bottom);    // Might crash program
    return *this;
}


/* ***************** end of boolean operators ******************* */

// The so-called DRIVER (or tester)
int main(){

    Fraction half(1,2);
    Fraction third(1,3);
    Fraction almost_one(1023,1024);

    cout << "One half: " << half << '\n';
    cout << "One third: " << third << '\n';
    cout << "Almost one: " << almost_one << "\n\n";

    // Unary minus
    cout << "Unary minus:\n";
    cout << "-(1/2) = " << -half << '\n';
    cout << "-(1/2) = " << half.operator-() << "\n\n";

    // Addition
    cout << "Addition:\n";
    cout << "1/2 + 1/3 = " << half + third << '\n';
    cout << "1/2 + 1/3 = " << operator+( half, third ) << '\n';
    cout << almost_one + Fraction(1,1024) << " = 1\n\n";

    // Binary minus (subtraction)
    cout << "Subtraction:\n";
    cout << "1/2 - 1/3 = " << half - third << '\n';
    cout << "1/2 - 1/3 = " << operator-(half, third) << "\n\n";

    // Multiplication
    cout << "Multiplication:\n";
    cout << "1/2 * 1/3 = " << half * third << '\n';
    cout << "1/2 * 1/3 = " << operator*( half, third ) << "\n\n";

    // Division (see multiplication above)
    cout << "Division:\n";
    cout << "1/2 / 1/3 = " << half / third << '\n';
    cout << "1/2 / 1/3 = " << operator/( half, third ) << "\n\n";;

    // Binary minus
    cout << "1/2 - 1/3 = " << half - third << '\n';
    cout << "1/2 - 1/3 = " << operator-(half, third) << "\n\n";

    // Mixed types
    cout << "Implicit conversions:\n";
    cout << "One plus half = " << 1 + half << '\n';
    cout << "half - 1 = " << half - 1 << '\n';
    cout << "almost_one * 1024 = " << almost_one * 1024 << '\n';
    cout << "1024 / almost_one = " << 1024 / almost_one << "\n\n";

    if ( half > 1  )
        cout << half << " > " << 1 << " ??? (This makes no sense).\n";


    int x = 2;
    int y = 2018;

    y += x += 5;

    cout << y << " " << x << std::endl;


    // half+= 1 ; // half.operator+=(1);
    cout << "half + third = " << ( half += third ) << std::endl;
    cout << "half??? = " << half << std::endl;


    x = 7;
    cout << x++ << std::endl;
    cout << x << std::endl;

    x = 7;
    cout << ++x << std::endl;
    cout << x << std::endl;

    // cout << ---x << std::endl;    // Does not work because unary minus precedes
                                     // the decrement operator
    cout << - --x << std::endl;      // This one is OK!

    cout << + ++x << std::endl;      // Contrary to what I thought, unary plus
                                     // is perfectly well defined.
    // cout << +++x << std::endl;    // Does not work either. Why???

    return 0;
}
