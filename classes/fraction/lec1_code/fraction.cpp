class Fraction{
  private:
    int numer;
    int denom;

  public:
    Fraction();
    Fraction( int );
    Fraction( int, int );
};

Fraction::Fraction(){
    int numer = 0;
    int denom = 1;
}

Fraction::Fraction( int n ){
    // number = n;    // <-- Oops ...
    numer = n;
    denom = 1;
}

Fraction::Fraction( int n, int d ){
    // number = n;    // <-- ... I did it again!!!
    numer = n;
    denom = d;
}

int main(){

    Fraction f;       // <-- Holds garbage values
    Fraction g(7);    // [ g = 7/1 ]
    Fraction h(2,3);  // [ h = 2/3 ]

    return 0;
}
