#include <iostream>          // std::cout, std::ostream
#include <fstream>           // std::ofstream

// using statements
using std::cout;
using std::ostream;
using std::ofstream;

class Fraction{
  private:
    int numer;
    int denom;

  public:
    // constructors (with default arguments)
    Fraction( int /* n */ = 0, int /* d */ = 1 );
    // These are no longer needed
    //     Fraction();
    //     Fraction( int );

    // printers
    void print() const;
    void print_to( ostream& /* out */ = cout ) const;

    // getters
    int get_numerator() const;
    int get_denominator() const;

    // setters
    void set_numerator( int );
    void set_denominator( int );

    // non-`const` versions
    int& numerator();
    int& denominator();

    // `const` versions
    int numerator() const;
    int denominator() const;
};


// One constructor to rule them all...
Fraction::Fraction( int n, int d ){
    numer = n;
    denom = d;
}
// one constructor to find them.
// One constructor to bring them all,
// and in the darkness bind them.
// In the land of PIC 10B, where the shadows lie.

// The constructors below are no longer needed.
//     Fraction::Fraction(){
//         numer = 0;
//         denom = 1;
//     }
//     Fraction::Fraction( int n ){
//         numer = n;
//         denom = 1;
//     }


// printers
void Fraction::print( ) const {
    // reusing code [single point of maintenance]
    print_to( cout );
    return;
}
// Default argument
void Fraction::print_to( ostream& out ) const {
    out << numer << '/' << denom;
    return;
}

// getters
int Fraction::get_numerator() const {
    return numer;
}
int Fraction::get_denominator() const {
    return denom;
}


// setters
void Fraction::set_numerator( int n ) {
    numer = n;
    return;
}
void Fraction::set_denominator( int d ) {
    denom = d;
    return;
}

// the other getter/setter functions
int& Fraction::numerator() {
    return numer;
} 
int& Fraction::denominator() {
    return denom;
} 
int Fraction::numerator() const {
    return numer;
} 
int Fraction::denominator() const {
    return denom;
} 


// The so-called DRIVER (or tester)
int main(){

    Fraction f;
    Fraction g(7);
    Fraction h(2,3);


    // Output file stream (setup)
    std::ofstream fout;           // `fout` = file out
    fout.open("output.txt");

    // Value of `f` to file: "output.txt"
    fout << "f = ";
    f.print_to( fout );
    fout << '\n';

    // Output file stream (clean up)
    fout.close();


    // Value of `g` to console [via default value]
    cout << "g = ";
    g.print_to();
    cout << '\n';

    h.numerator() = 2018;

    // Value of `h` to console [via `print_to( cout )`]
    cout << "h = ";
    h.print();
    cout << '\n';


    return 0;
}
