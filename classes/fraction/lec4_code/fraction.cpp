#include <iostream>          // std::cout, std::ostream
#include <fstream>           // std::ofstream

// using statements
using std::cout;
using std::ostream;
using std::ofstream;

class Fraction{
  private:
    int numer;
    int denom;

  public:
    // constructors (with default arguments)
    Fraction( int /* n */ = 0, int /* d */ = 1 );
    // Note(s):
    //  *  The place to set the default aguments is the function declaration
    //  *  The name of the parameters are not needed; only the types and values

    // printers [deprecated] (see operator<<)
    // void print() const;
    // void print_to( ostream& /* out */ = cout ) const;

    // getters & setters [deprecated] (see numerator() & denominator())
    // int get_numerator() const;
    // int get_denominator() const;
    // void set_numerator( int );
    // void set_denominator( int );


    int& numerator();
    int& denominator();
    int numerator() const;
    int denominator() const;


    // Friend **NON-MEMBER** functions and operators
    friend ostream& operator<<( ostream&, const Fraction& );
    friend Fraction operator+( const Fraction&, const Fraction& );
};


// One constructor to rule them all...
Fraction::Fraction( int n, int d ){
    numer = n;
    denom = d;
}


// printers
// void Fraction::print( ) const {
//     // reusing code [single point of maintenance]
//     print_to( cout );
//     return;
// }
// Default argument (set at the function declaration [above])
// void Fraction::print_to( ostream& out ) const {
//     out << numer << '/' << denom;
//     return;
// }

// getters
// int Fraction::get_numerator() const {
//     return numer;
// }
// int Fraction::get_denominator() const {
//     return denom;
// }


// setters
// void Fraction::set_numerator( int n ) {
//     numer = n;
//     return;
// }
// void Fraction::set_denominator( int d ) {
//     denom = d;
//     return;
// }

// the other getter/setter functions
int& Fraction::numerator() {
    return numer;
} 
int& Fraction::denominator() {
    return denom;
} 
int Fraction::numerator() const {
    return numer;
} 
int Fraction::denominator() const {
    return denom;
} 



// Non-member operators
ostream& operator<<( ostream& out, const Fraction& f ){
    out << f.numer << '/' << f.denom;                   // Fails if not friend
    // out << f.numerator() << '/' << f.denominator();  // Works
    return out;
}

// This is a 'one-liner' that might be hard to understand/maintain
//     Fraction operator+( const Fraction& a, const Fraction& b ){
//         return Fraction(a.numer*b.denom + b.numer*a.denom, a.denom*b.denom);
//     }

// This is a much longer, but self-explaining implementation
Fraction operator+( const Fraction& a, const Fraction& b ){
    // Recall:
    //     p/q + r/s = ( p*s + q*r ) / q*s;

    int p = a.numerator();
    int q = a.denominator();
    int r = b.numerator();
    int s = b.denominator();

    int top = p*s + q*r;
    int bottom = q*s;

    return Fraction(top,bottom);
}
// Note: Friendship is not needed here! Why not???



// The so-called DRIVER (or tester)
int main(){

    Fraction f;
    Fraction g(7);
    Fraction h(2,3);


    // Output file stream (setup)
    std::ofstream fout;           // `fout` = file out
    fout.open("output.txt");

    // Value of `f` to file: "output.txt"
    //     fout << "f = ";
    //     f.print_to( fout );
    //     fout << '\n';
    //
    // Same as above but using operator<<
    fout << "f = " << f << '\n';

    // Output file stream (clean up)
    fout.close();


    // Value of `g` to console [via default value]
    //     cout << "g = ";
    //     g.print_to();
    //     cout << '\n';
    //
    // Same as above but using operator<<
    cout << "g = " << g << '\n';

    h.numerator() = 2018;

    // Value of `h` to console [via `print_to( cout )`]
    //     cout << "h = ";
    //     h.print();
    //     cout << '\n';
    // Same as above but using operator<<
    cout << "h = " << h << "\n\n";


    Fraction half(1,2);
    Fraction third(1,3);
    Fraction almost_one(1023,1024);

    cout << "One half: " << half << '\n';
    cout << "One third: " << third << '\n';
    cout << "Almost one: " << almost_one << "\n\n";

    cout << "1/2 + 1/3 = " << half + third << '\n';
    cout << almost_one + Fraction(1,1024) << " = 1\t(O.o)\n";

    return 0;
}
