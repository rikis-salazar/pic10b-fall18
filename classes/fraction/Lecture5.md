% Lecture 5 (Summary)
% Ricardo Salazar
% January 19, 2017


# The theory 

## During lecture we

 *  used an initialization list to improve the efficiency of our class
    constructor.

 *  introduce the `this` pointer.

     -  `this` holds the memory address of its associated object.

     -  to recover the object from `this`, the pointer needs to be
        _de-referenced_ via `operator*`, as in `*this`.

     -  to access members:
     
        > `(*this).member_field`  
        > `(*this).member_function(...)`

     -  alternatively:
     
        > `this->member_field`  
        > `this->member_function(...)`
    

## During lecture we (cont)

 *  pointed out that member functions/operators, _always receive at least one
    parameter_.
    
     -  Even if the signature lists no parameters, they _implicitly receive_
        its associated object via `this`.

 *  coded member and non-member operators, then compared the freebies they
    provide.
 
     -  non-member `operator+` takes full advantage of _implicit conversion_ to
        _give away two freebies_:

        > `Fraction` plus `int`, and `int` plus `Fraction`.

     -  member `operator*` was only able to _give one freebie away_, namely
     
        > `Fraction` times `int`.

## Concepts we came across:

 i. **pointer**:

    > a primitive type in `C++` that references a location in memory.

 i. **dereferencing**:
 
    > the act of obtaining the value stored at a memory location via a pointer.

 i. **implicit conversion**:  
 
    > when a function expects a `type1` parameter, but receives a `type2`
    > parameter, the compiler will produce valid code if there is a valid
    > constructor `type1::type1(type2)`, or a valid _conversion operator_,
    > as long as the keyword `explicit` is not used.
    

## The goals

We want to: 

 i. Code all missing _arithmetic_ operators: `+=`, `-=`, ... `++`, `++`, `--`,
    and `--`.

     *  No. There is no typo above.

 i. Implement all boolean operators sticking to the principle of _single point
    of maintenance_.

 i. Make sure our _numeric_ class behaves the way other numeric classes do.

 i. Be able to _count_ all `Fraction` objects created within a driver.

 i. Move on to other classes... `Fraction`s are getting boring. Aren't they?


# The code

## The interface

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class Fraction{
    ...

  public:
    ...

    // Member `operator*`. Param.: one implicit, one explicit.
    Fraction operator*( const Fraction& ) const ;
    ...

  // Non-member `operator+`. Param.: two explicit.
  friend Fraction operator+( const Fraction&, const Fraction& );
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Function definitions

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
// Efficient constructor
Fraction::Fraction( int n, int m ) : numer(n), denom(m) {
    // normalize();   // Eliminates common factors.
}
    

Fraction operator+( const Fraction& lhs, const Fraction& rhs ){
    int p = lhs.numer;
    int q = lhs.denom;
    int r = rhs.numer;
    int s = rhs.denom;
    return Fraction(p*s + q*r, q*s);
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Function definitions (cont)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
Fraction Fraction::operator+( const Fraction& rhs ) const {
    // (*this) is the left hand side (lhs) parameter

    // int p = *this.numer; // Error: `.` precedes `*`
    int p = (*this).numer;  // OK
    int q = this->denom;    // `ptr->` is shortcut for `(*ptr).`
    // int q = denom;       // same as `this->denom`

    int r = rhs.numer;
    int s = rhs.denom;

    return Fraction(p*r, q*s);
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# Other notes

## Miscellaneous notes

 *  If a constructor is marked `explicit`, no implicit conversion  is allowed.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class PickyFraction{
  public:
    explicit PickyFraction( int );
    PickyFraction( Fraction );
    ...
};
PickyFraction operator+(PickyFraction, PickyFraction);

void some_fun(){
  // cout << PickyFraction(3) + 1;           // No can do!!!
  cout << PickyFraction(3) + Fraction(1,2);  // OK
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
## Miscellaneous notes (cont)

 *  If you must code the companion `operator*` to handle `int` _times_
    `Fraction`, reuse instead of repeat [previous code].

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
// Bad: error prone, and time consuming.
// Fraction operator*( int p, const Fraction& rhs ){
//     int q = 1;
//     int r = rhs.numerator();
//     int s = rhs.denominator();
//     ... (copy-paste from member `operator*`)
// }

// Good: single point of maintenance + readable code.
Fraction operator*( int p, const Fraction& rhs ){
    return rhs * p;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Resources

## Code from lecture

|**File**|**Description**|**URL**|
|:---------------:|:------------------------------------------|:---------------|
| [`fraction.cpp`][00-example] | Code from lecture. | [`cpp.sh/3ddi5`][00-url] |
| [`cle...on.cpp`][01-example] | Code from lecture (clean version). | [`cpp.sh/9k65`][01-url] |

[00-example]: lec5_code/fraction.cpp
[01-example]: lec5_code/clean-fraction.cpp
[00-url]: http://cpp.sh/3ddi5
[01-url]: http://cpp.sh/9k65
