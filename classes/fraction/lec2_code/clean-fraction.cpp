#include <iostream>          // std::cout

// using namespace std;      // We'll try to avoid **full** namespaces
using std::cout;

class Fraction{
  private:
    int numer;
    int denom;

  public:
    // constructors
    Fraction();
    Fraction( int );
    Fraction( int, int );

    // printers
    void print() const ;
    void print_to( /*some_object*/ ) const; // want to send `cout` as parameter

    // getters
    int get_numerator() const ;
    int get_denominator() const ;

    // setters
    void set_numerator( int );
    void set_denominator( int );

    // non-`const` versions
    int& numerator();
    int& denominator();

    // `const` versions
    int numerator() const;
    int denominator() const;
};


// constructors
Fraction::Fraction(){
    numer = 0;
    denom = 1;
}
Fraction::Fraction( int n ){
    numer = n;
    denom = 1;
}
Fraction::Fraction( int n, int d ){
    numer = n;
    denom = d;
}


// printers
void Fraction::print() const {
    cout << numer << "/" << denom;
    return;
}
void Fraction::print_to() const {
    // This is just a _stub function_
    return;
}

// getters
int Fraction::get_numerator() const {
    return numer;
}
int Fraction::get_denominator() const {
    return denom;
}


// setters
void Fraction::set_numerator( int n ) {
    numer = n;
    return;
}
void Fraction::set_denominator( int d ) {
    denom = d;
    return;
}

// the other getter/setter functions
int& Fraction::numerator() {
    return numer;
} 
int& Fraction::denominator() {
    return denom;
} 
int Fraction::numerator() const {
    return numer;
} 
int Fraction::denominator() const {
    return denom;
} 



// The so-called DRIVER (or tester)
int main(){

    Fraction f;       // [ f = 0/1 ]
    Fraction g(7);    // [ g = 7/1 ]
    Fraction h(2,3);  // [ h = 2/3 ]

    f.print();
    cout << '\n';

    h.numerator() = 2018;
    h.print();
    cout << '\n';

    return 0;
}
