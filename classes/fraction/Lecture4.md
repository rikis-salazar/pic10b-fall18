% Lecture 4 (Summary)
% Ricardo Salazar
% January 17, 2017


# The theory 

## What we did

During lecture we

 *  [finally] overloaded `operator<<` to display `Fraction` objects to output
    streams (_e.g.,_ `cout`).

 *  implemented a _non-member_ addition operator (`operator+`)...

    - the "clever" way (_i.e.,_ super-short-and-hard-to-understand); and
    - the "right" way (_i.e.,_ self-commenting-and-easy-to-follow way).

 *  learned that "friends" of a class are granted _special permissions_ to
    access [all] member fields.


## Concepts we came across:

 i. **friendship**:

    > Non-member functions, or classes, that are included in a class interface
    > with the keyword `friend`, can access all members of the class.

 i. **unavoidable return by reference**:
 
    > If an object cannot be copied and it is to be returned by a function,
    > there is no way around it: the return has to be by reference.

 i. **chaining**:  
 
    > invocation of multiple function calls. The return object is, in turn, sent
    > as parameter to the next function call.
    

## The goals

We want to: 

 i. Make our constructor more efficient via an **initialization list**.

 i. Implement all boolean operators sticking to the concept of _single point of
    maintenance_.

 i. Understand the difference between "unary" and "binary" operators. E.g.:

    - Unary minus. `-Fraction(1,2);` equivalent to `Fraction(-1,2)`.
    - Binary minus. Also known as subtraction operator.

 i. Introduce `this`. [_Wait! Whaaaaat?_]


# The code

## The interface

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class Fraction{
  private:
    int numer;
    int denom;

  public:
    ...

  // Non-members (below)
  friend ostream& operator<<( ostream&, const Fraction& );
  friend Fraction operator+( const Fraction&, const Fraction& );
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Function definitions

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
friend ostream& operator<<( ostream& out, const Fraction& f ){
    out << f.numer << '/' << f.denom;   // Works b/c friend
    return out;
}

// DO NOT CODE LIKE THIS!!! (Please)
// It is not easy to know what this does unless your are
// familiar with the technical details of the class.
// 
// Fraction operator+( const Fraction& a, const Fraction& b ){
//     return Fraction(a.numer*b.denom+a.denom*b.numer,
//                     a.denom*b.denom);
// }
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Function definitions (cont)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
// Code like this instead (Please) 
// [Also, sorry for yelling.]
Fraction operator+( const Fraction& a, const Fraction& b ){
    // Recall:    p/q + r/s = ( p*s + q*r ) / q*s
    int p = a.numer;
    int q = a.denom;
    int r = b.numer;
    int s = b.denom;

    int top = p*s + q*r;
    int bottom = q*s;

    return Fraction(top, bottom);
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# Other notes

## Miscellaneous notes

 *  _Printer, setter, and getter functions_ are now deprecated[^three].
 
    - `operator<<` replaces both _printers_.
    - `numerator()` and `denominator()` can be used as _getters, or setters_.

 *  Operators come in many different flavors: _friends_, ~~_foes_~~
    _non-friends_, _members_, _non-members_, _unary_, _binary_, etc.

 *  Our implementation of `operator+` comes with "freebies[^four]".

     -  Had we chosen to implement it as a member, we would have had to code
        other _companion_ operators.
      
 *  Our wishlist remains the same for now (see Lecture 3).

[^three]: Slated for deletion and no longer maintained. _Use at your own risk!_
[^four]: _Freebie_.- a thing given free of charge.


## Experiment!

Note that our interface has the friend declarations

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
friend ostream& operator<<( ... );
friend Fraction operator+( ... );
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Aren't you wondering why `operator+` returns by value instead of by
reference (like `operator<<`)?

* Care to change the return type and let us know about any issues you
encounter?

And remember

> _"With great power comes great responsability."_

# Resources

## Code from lecture

|**File**|**Description**|**URL**|
|:---------------:|:------------------------------------------|:---------------|
| [`fraction.cpp`][00-example] | Code from lecture. | [`cpp.sh/333lc`][00-url] |
| [`cle...on.cpp`][01-example] | Code from lecture (clean version). | [`cpp.sh/4sayw`][01-url] |

[00-example]: lec4_code/fraction.cpp
[01-example]: lec4_code/clean-fraction.cpp
[00-url]: http://cpp.sh/333lc
[01-url]: http://cpp.sh/4sayw
