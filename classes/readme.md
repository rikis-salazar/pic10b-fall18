# _Home-made_ classes

During this quarter we will be coding some "standard" classes from scratch. The
idea is for us to learn how _things work under the hood_. Please note that our
versions will likely be _much weaker_ than other similarly-named classes that
provide better functionality.

*   The template `pic10b::DoublyLinkedList<T>` class: this material was started
    during the _2016 Summer_ quarter. It attempts to explain the ideas and
    technical concepts behind a doubly linked list container.

    -   [The `pic10b::DoublyLinkedList<T>`: handout][list-h]
    -   [The `pic10b::DoublyLinkedList<T>`: code][list-c]

*   The non-template `pic10b::vector` class: this material was started during
    the _2016 Summer_ quarter. It attempts to explain the ideas and technical
    concepts behind a vector container.

    -   [A non-template `pic10b::vector`: handout][vector-h]
    -   [A non-template `pic10b::vector`: code][vector-c]

*   The `Thing` class: this material was started during the _2016 Summer_
    quarter. It attempts to explain the ideas and technical concepts behind the
    so-called _Big 4_.

    -   [A `Thing` class: handout][thing]
    -   [A `Thing` class: code][thing-code]

*   The `Cosa` class: this material was generated during the _2018 Winter_
    quarter. The code from the slides corresponds to a hybrid of the code we
    discussed during lecture. In particular, for the coming midterm, feel free
    to ignore parts of the slides related to `static` fields/functions.

    Use the [slides (summary)][cosa-slide] to review concepts related to the
    big 4 (_e.g.,_ what are they and what are their signatures).

    -   Lecture 1: [summary (slides)][cosa-slide], [code][cosa-code],
        [code (clean version)][cosa-clean-code].

*   The `Fraction` class: the material posted here was generated during the
    _2018 Winter_ quarter at UCLA. In particular, the slides might contain
    statements along the line of
                          
    > _during lecture, we discussed X notion, ... we also coded Y function..._ 

    While technically those statements might actually be false, rest assured
    that all topics/functions mentioned in these slides were, or will be,
    covered at some point during this current quarter.

    -   Lecture 1: [summary (slides)][lec1-slide], [code][lec1-code].
    -   Lecture 2: [summary (slides)][lec2-slide], [code][lec2-code],
        [code (clean version)][lec2-clean-code].
    -   Lecture 3: [summary (slides)][lec3-slide], [code][lec3-code],
        [code (clean version)][lec3-clean-code], [output file][out3].
    -   Lecture 4: [summary (slides)][lec4-slide], [code][lec4-code],
        [code (clean version)][lec4-clean-code].
    -   Lecture 5: [summary (slides)][lec5-slide], [code][lec5-code],
        [code (clean version)][lec5-clean-code].
    -   Lecture 6: [summary (slides)][lec6-slide], [code][lec6-code],
        [code (clean version)][lec6-clean-code].

Additionally, take a look at [this mostly equivalent implementation][eq] of our
`Fraction` class. It corresponds to the approach I followed during the 2017
summer session. Notice that unlike the code in the bullet points above, here

i.  the arithmetic operators (`operator+`, `operator-`, etc.) are implemented in
    terms of the _shortcut-like_ member ones (`Fraction::operator+=`,
    `Fraction::operator-=`, etc.), and

ii. the _helper_ functions `Fraction::normalize()`, as well as `Fraction::gcd()`
    have been implemented.

[lec1-slide]: ./fraction/Lecture1.md.pdf
[lec2-slide]: ./fraction/Lecture2.md.pdf
[lec3-slide]: ./fraction/Lecture3.md.pdf
[lec4-slide]: ./fraction/Lecture4.md.pdf
[lec5-slide]: ./fraction/Lecture5.md.pdf
[lec6-slide]: ./fraction/Lecture6.md.pdf
[lec1-code]: ./fraction/lec1_code/fraction.cpp
[lec2-code]: ./fraction/lec2_code/fraction.cpp
[lec3-code]: ./fraction/lec3_code/fraction.cpp
[lec4-code]: ./fraction/lec4_code/fraction.cpp
[lec5-code]: ./fraction/lec5_code/fraction.cpp
[lec6-code]: ./fraction/lec6_code/fraction.cpp
[lec2-clean-code]: ./fraction/lec2_code/clean-fraction.cpp
[lec3-clean-code]: ./fraction/lec3_code/clean-fraction.cpp
[lec4-clean-code]: ./fraction/lec4_code/clean-fraction.cpp
[lec5-clean-code]: ./fraction/lec5_code/clean-fraction.cpp
[lec6-clean-code]: ./fraction/lec6_code/clean-fraction.cpp
[out3]: ./fraction/lec3_code/output.txt
[eq]: ./fraction/three-file-layout

[cosa-slide]: ./cosa/Lecture1.md.pdf
[cosa-code]: ./cosa/lec1_code/cosa.cpp
[cosa-clean-code]: ./cosa/lec1_code/clean-cosa.cpp

[thing]: ../handouts/the-big-4.html
[thing-code]: ../handouts/big4-files/thing.cpp
[vector-h]: ../handouts/non-template-vector.html
[vector-c]: https://bitbucket.org/rikis-salazar/10b-hw-pic10b-vector/raw/master/src/pic10b_vector_non_template.cpp
[list-h]: ../handouts/template-list.html
[list-c]: ../handouts/dl-list-files/

---

[Return to main PIC course website][main]

[main]: ..
