#include <iostream>

class Cosa{
  private:
    int id_number;

    // static variables must be initialized outside of the class declaration.
    static int the_count;    

  public:
    // The big 4
    Cosa();
    Cosa( const Cosa& );
    Cosa& operator=( const Cosa& );
    ~Cosa();

    // regular member function(s)
    int get_id_number() const;

    // static member function(s)
    static int get_count();
    static void set_count(int);
};

// Initialization of static field(s)
int Cosa::the_count = 0;


// Default constructor
Cosa::Cosa() : id_number(++the_count) {
    std::cout << "[Default] constructing Cosa " << id_number << ".\n";
}
// Alternatively, we can code the less efficient version
//     Cosa::Cosa() {
//         the_count += 1;
//         id_number = the_count;
//         std::cout << "[Default] constructing Cosa " << id_number << ".\n";
//     }

// Copy constructor
Cosa::Cosa( const Cosa& c ) : id_number(++the_count) {
    std::cout << "[Copy] constructing Cosa " << id_number 
              << ", from Cosa "<< c.id_number << ".\n";
}

// Assignment operator
Cosa& Cosa::operator=( const Cosa& c ){
    std::cout << "Assigning the \"contents\" of Cosa " << c.id_number
              << " to Cosa " << id_number << "\n";
    return *this;
}

// Destructor
Cosa::~Cosa(){
    std::cout << "Destructing Cosa " << id_number << ".\n";
}


// "regular" member function (getter)
int Cosa::get_id_number() const {
    return id_number;
}


// Static member functions
void Cosa::set_count( int new_count ){
    the_count = new_count;
    return ;
}
int Cosa::get_count(){
    return the_count;
}


// Non-member functions
void by_value_fun( Cosa c ) {
    std::cout << "This function receives a `Cosa` by value. ";
    std::cout << "Its ID is " << c.get_id_number()
              << ".\nThe total number of `Cosa` objects created so far is "
              << Cosa::get_count() << ".\n";
    return;
}

void by_reference_fun( Cosa& c ) {
    std::cout << "This function receives a `Cosa` by reference. ";
    std::cout << "Its ID is " << c.get_id_number()
              << ".\nThe total number of `Cosa` objects created so far is "
              << Cosa::get_count() << ".\n";
    return;
}


int main() {
    // Default constructor
    Cosa c1;

    // Copy constructor(s)
    Cosa c2 = c1;
    Cosa c3(c2);

    std::cout << "\nCalling `by_value_fun(c3)`...\n";
    by_value_fun(c3);
    std::cout << "Returning to `main()`...\n";

    std::cout << "\nCalling `by_reference_fun(c3)`...\n";
    by_reference_fun(c3);
    std::cout << "Returning to `main()`...\n\n";

    // Assignment operator
    c1 = c3;

    int oldCosaCount = Cosa::get_count();
    Cosa::set_count( 2018 + oldCosaCount );
    std::cout << "\nAssuming we create 2018 new Cosas, the total count would "
              << "be " << c1.get_count() << ".\n";

    Cosa::set_count( oldCosaCount );
    std::cout << "Since it was just an assumption the actual count remains at "
              << "its old value, namely " << oldCosaCount << ".\n";

    std::cout << "\nIf we create another Cosa, the count is two lines below:\n";
    std::cout << Cosa().get_count() << " <-- (here is the count)\n";

    std::cout << "\nDestructing all remaining `Cosa` objects...\n\n";
    return 0;
}

/**

Output:

    [Default] constructing Cosa 1.
    [Copy] constructing Cosa 2, from Cosa 1.
    [Copy] constructing Cosa 3, from Cosa 2.

    Calling `by_value_fun(c3)`...
    [Copy] constructing Cosa 4, from Cosa 3.
    This function receives a `Cosa` by value. Its ID is 4.
    The total number of `Cosa` objects created so far is 4.
    Destructing Cosa 4.
    Returning to `main()`...

    Calling `by_reference_fun(c3)`...
    This function receives a `Cosa` by reference. Its ID is 3.
    The total number of `Cosa` objects created so far is 4.
    Returning to `main()`...

    Assigning the "contents" of Cosa 3 to Cosa 1

    Assuming we create 2018 new Cosas, the total count would be 2022.
    Since it was just an assumption the actual count remains at its old value, namely 4.

    If we create another Cosa, the count is two lines below:
    [Default] constructing Cosa 5.
    5 <-- (here is the count)
    Destructing Cosa 5.

    Destructing all remaining `Cosa` objects...

    Destructing Cosa 3.
    Destructing Cosa 2.
    Destructing Cosa 1.

**/
