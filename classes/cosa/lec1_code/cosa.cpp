#include <iostream>

class Cosa{
  private:
    // static variables must be initialized outside of the class declaration.
    // As we saw during lecture, failing to do so, causes the linker not to
    // be able to put the program together.
    static int the_count /* = 0 */;    

  public:
    // Def. constr
    Cosa();
    // Copy constr
    Cosa( const Cosa& );
    // Assign. oper
    Cosa& operator=( const Cosa& );
    // Def. constr
    ~Cosa();

    static int get_count();
    static void set_count(int);    // <-- This is the type of function you
                                   //     need in assignment 2.
};

int Cosa::the_count = 0;


void Cosa::set_count( int new_count ){
    the_count = new_count;
    return ;
}

int Cosa::get_count(){
    return Cosa::the_count;
}
Cosa::Cosa(){
    the_count++;
    std::cout << "Default constr. called....\n";
}

Cosa::Cosa( const Cosa& c ){
    the_count++;
    std::cout << "Copy constr. called....\n";
}

Cosa& Cosa::operator=( const Cosa& c ){
    std::cout << "Assn. oper. called....\n";
    return *this;
}

Cosa::~Cosa(){
    std::cout << "Destr. called....\n";
}


void cosa_fun( Cosa by_alue ) {
    std::cout << "********************Hola, mundo.\n";
}


int main() {
    Cosa c;      // Default constr.  Big 1.

    Cosa d = c;  // Copy constructor. Big 2.

    d = c ;      // same as d.operator=( c ); Big 3.

    cosa_fun( d );

    std::cout << "Cosas created = " << d.get_count() << '\n';
    std::cout << "Cosas created = " << Cosa().get_count() << '\n';
    std::cout << "Cosas created = " << Cosa::get_count() << '\n';

    return 0;
}
