% `Cosa` class: Lecture 1 (Summary)
% Ricardo Salazar
% January 26, 2017


# The theory 

## During lecture we

 *  identified the compiler provided functions for our `Cosa` class. They are

     -  the _default constructor_,
     -  the _copy constructor_,
     -  the _assignment operator_, and
     -  the _destructor_.

     Together they are known as **the big 4**. 

 *  figured out the correct signatures for the functions above.

 *  solved the _counting-objects-belonging-to-a-class_ problem by using a
    `static` member field.

## Concepts we came across:

 i. **the big 4**:

    > if a class defines either of the destructor, copy constructor, or
    > assignment operator, then it should not rely on any of the default
    > implementations provided by the compiler.
    
 i. **static member fields**:
 
    > a static data is not available as separate copy with each object of the
    > class it belongs to. They are not initialied using a constructor, because
    > they are not dependent on object initialization.

 i. **static member functions**:
 
    > they work for the class as whole rather than for a particular object.
    
# The code

## The interface

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class Cosa{
  private:
    int id_number;
    static int the_count;

  public:
    Cosa();
    Cosa( const Cosa& );
    Cosa& operator=( const Cosa& );
    ~Cosa();
    int get_id_number() const;
    static int get_count();
    static void set_count( int );
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Function definitions

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
// If static fields are not properly initialized, we risk
// getting linker errors... I've been told :P
int Cosa::the_count = 0;

// Default constructor
Cosa::Cosa() : id_number(++thecount) {
    cout << "Default constructor...\n";
}

// Copy constructor
Cosa::Cosa( const Cosa& c ) : id_number(++thecount) {
    cout << "Copy constructor...\n";
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Function definitions (cont)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
// Assignment operator
Cosa& Cosa::operator=( const Cosa& rhs ){
    cout << "Assignment operator...\n";
    return *this;
}

// Destructor
Cosa::~Cosa() {
    cout << "Destructor...\n";
}

// Fun fact: There is yet another constructor. Its name is
// the move constructor, and has signature
//     Cosa::Cosa( Cosa&& )
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Function definitions (cont)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
// Regular getter should be marked `const`
int Cosa::get_id_number() const {
    return id_number;
}

// Static getter cannot be marked `const`
int Cosa::get_count() {
    return the_count;
}

// Static setter
void Cosa::set_count( int n ) {
    the_count = n;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# Other notes

## Do we really need the big 4?

In most cases, the compiler provided constructors, assignment operator, and
destructor, is more than enough to correctly model real world objects. However, 
when dealing with some _data structures_ the compiler provided functions are not
appropriate.

In this course we'll code the big 4 for some of the following _containers_:

 *  our very own _vector_ (`Pic10b::vector<...>`),
 *  a _doubly linked list_,
 *  a primitive _stack_, and
 *  a _binary search three_.
 

# Resources

## Code from lecture

|**File**|**Description**|**URL**|
|:---------------:|:------------------------------------------|:---------------|
| [`cosa.cpp`][00-example] | Code from lecture. | [`cpp.sh/6bnlz`][00-url] |
| [`cle...sa.cpp`][01-example] | Code from lecture (clean version). | [`cpp.sh/8pj72`][01-url] |

[00-example]: lec1_code/cosa.cpp
[01-example]: lec1_code/clean-cosa.cpp
[00-url]: http://cpp.sh/6bnlz
[01-url]: http://cpp.sh/8pj72
